<?php

function setForm($formNID=null, $formSID=null){//, $epdqSettings){
  // drupal_set_message("setForm NID: {$formNID}");
  // drupal_set_message("setForm SID: {$formSID}");

  variable_set('BarclaysFields', array('N/A', 'FirstName', 'LastName', 'AddressLine#1', 'AddressLine#2', 'AddressLine#3', 'Town/City', 'PostCode', 'Country', 'Email', 'Mobile#', 
        'Cost', 'Cost-Other', 'Payment-Status'));

  /*$epdqSettings   = _epdq_get_components_settings($formNID);
  dpr($epdqSettings);*/

  // drupal_set_message("Tsting cost: " . form_data('Cost', $formNID, $formSID));

  $order = uc_order_new(0, 'completed');
  $order->delivery_first_name     = form_data('FirstName', $formNID, $formSID);
  $order->delivery_last_name      = form_data('LastName', $formNID, $formSID);
  $order->delivery_street1        = form_data('AddressLine#1', $formNID, $formSID);
  $order->delivery_street2        = form_data('AddressLine#2', $formNID, $formSID);
  $order->delivery_city           = form_data('Town/City', $formNID, $formSID);
  $order->delivery_postal_code    = form_data('PostCode', $formNID, $formSID);
  $order->delivery_country        = form_data('Country', $formNID, $formSID);
  $order->delivery_phone          = form_data('Mobile#', $formNID, $formSID);
  $order->delivery_primary_email  = form_data('Email', $formNID, $formSID);
  // $data['total']                  = form_data('Cost', $formNID, $formSID);
  /***************************************/
    $eventnode = _eventform_parent_node($formNID, true);

    /*******************************************
     * incase this isn't an event & event-form 
     * relationship and it's a simple barclays
     * enabled webform.
     * PS: I enable ubercart on the 'Donate Now'
     * form to minimize changes to the existing
     * code
     *******************************************/
    if(!$eventnode){
      $eventnode = node_load($formNID);
      $amount    = form_data('Cost', $formNID, $formSID);
      $other     = form_data('Cost-Other', $formNID, $formSID);
      $amount    = $other ? $other : $amount;
      $eventnode->price = 0.01;//$amount;
      // drupal_set_message("TEST Amount: {$amount}");

      // drupal_set_message("REMEMBER TO UNDO THE STATIC COST TEST : epdqOLD.inc", 'warning');
      // dsm($eventnode);
    }
    // if($eventnode){
      $product = node_load( $eventnode->nid );
      $product->qty = 1;
      $product->order_id = $order->order_id;
      drupal_write_record('uc_order_products', $product);
      $order->products[] = $product;
    /*}else{

    }*/
  /***************************************/
  // dsm($order, 'order');
  uc_order_save($order);

  // dsm($order, "checking order example in 'Barclays ePDQOLD'");
    // uc_order_delete($order->order_id);

  $selectedStore = get_barclaysepdq_node_settings($formNID);
  // dsm($selectedStore, 'selectedStore');
  $selectedStoreData = get_barclaysepdq_settings($selectedStore->sid);
  // dsm($selectedStoreData, 'form node epdq store settings');

  $data['clientid']     = $selectedStoreData->sid;// 3785;
  $data['password']     = $selectedStoreData->passphrase;// 'heathgate';
  $data['oid']          = $order->order_id;
  $data['chargetype']   = 'Auth';
  $data['currencycode'] = 826;
  $data['total']        = $eventnode->price;

  $data['epdqdata']             = str_replace('<INPUT name=epdqdata type=hidden value="', '' , uc_epdq_encrypt($order, $selectedStoreData));
  $data['epdqdata']             = str_replace('">', '', $data['epdqdata']);

  $data['cpi_logo']             = $selectedStoreData->cpilogo;
  $data['returnurl']            = $selectedStoreData->returnurl;
  $data['merchantdisplayname']  = $selectedStoreData->merchantname;
  $data['baddr1']               = $order->delivery_street1;
  $data['baddr2']               = $order->delivery_street2;
  $data['bfullname']            = $order->delivery_first_name . ' ' . $order->delivery_last_name;
  $data['bcity']                = $order->delivery_city;
  $data['bpostalcode']          = $order->delivery_postal_code;
  $data['bcountry']             = $order->delivery_country;
  $data['btelephonenumber']     = $order->delivery_phone;
  $data['email']                = $order->delivery_primary_email;

  $data['method']   = 'post'; // Just for clarification. This is the default value. 
  $data['action']   = fetchState($selectedStoreData->state);
  $data['enctype']  = "application/x-www-form-urlencoded";

  return $data;
}

/**************************
 * matching settings to
 * submitted data and 
 * return form data
 **************************/
/*function form_data($find=null, $formNID=0, $formSID=0){
// variable_set('BarclaysFields', array('N/A', 'FirstName', 'LastName', 'AddressLine#1', 'AddressLine#2', 'AddressLine#3', 'Town/City', 'PostCode', 'Country', 'Email', 'Mobile#', 'Cost'));

  $epdqSettings   = _epdq_get_components_settings($formNID);
  foreach ($epdqSettings as $setting) {
    if($setting->type == $find)
      break;
  }

  $submission = webform_menu_submission_load($formSID, $formNID);
  // dpr($submission);
  // print"<pre>";
  // print_r($setting);
  // print"</pre>";
  if($submission){
    // drupal_set_message("TESTING {$find}: {$submission->data[$setting->cid][0]}", 'error');
    return $submission->data[$setting->cid][0];
  }
}*/

function fetchState($state=null, $enq=false){
  // drupal_set_message("STATE: " . variable_get('uc_epdq_state', ''));

  switch($state){
    case 'LIVE':
      if($enq){
        $data['server'] = "secure2.epdq.co.uk";
        $data['url']    = "/cgi-bin/CcxBarclaysEpdqEncTool.e";
      }else{
        $data =  'https://secure2.epdq.co.uk/cgi-bin/CcxBarclaysEpdq.e';
      }
      break;
    default:
      if($enq){
        $data['server'] = "secure2.mde.epdq.co.uk";
        $data['url']    = "/cgi-bin/CcxBarclaysEpdqEncTool.e";
      }else{
        $data =  'https://secure2.mde.epdq.co.uk/cgi-bin/CcxBarclaysEpdq.e';
      }
      break;
  }

  // $data =  'https://secure2.mde.epdq.co.uk/cgi-bin/CcxBarclaysEpdq.e';
  return $data;
}

function uc_epdq_encrypt($order, $storeData) {

  //define the remote cgi in readiness to call pullpage function

  /*dsm($storeData);
  drupal_set_message("fetchState: " . fetchState($storeData->state));*/

  if($storeData->state == 'LIVE'){
    $server="secure2.epdq.co.uk";
    $url="/cgi-bin/CcxBarclaysEpdqEncTool.e";
  }else{
    $server="secure2.mde.epdq.co.uk";
    $url="/cgi-bin/CcxBarclaysEpdqEncTool.e";
  }
	// 'https://secure2.mde.epdq.co.uk/cgi-bin/CcxBarclaysEpdqEncTool.e'

  // $data   = fetchState(true);
  // $server = $data['server'];
  // $url    = $data['url'];

  //the following parameters have been obtained earlier in the merchant's webstore
  //clientid, passphrase, oid, currencycode, total
	$clientid       =   $storeData->sid;// 3785;
	$passphrase     =   $storeData->passphrase;// 'heathgate';
	$oid            =   $order->order_id;// 7;
	$chargetype     =   'Auth';
	$currencycode   =   826;
	// $total          =   100;

	$params="clientid=$clientid";
	$params.="&password=$passphrase";
	$params.="&oid={$order->order_id}";
	$params.="&chargetype=Auth";
	$params.="&currencycode=$currencycode";
	$params.="&total={$order->products[0]->price}";

  //perform the HTTP Post
  $response = uc_epdq_pullpage($server, $url, $params);

  //split the response into separate lines
  $response_lines = explode("\n", $response);

  //for each line in the response check for the presence of the string 'epdqdata'
  //this line contains the encrypted string
  $response_line_count = count($response_lines);
  $strEPDQ = '';
  for ($i = 0; $i < $response_line_count; $i++) {
    if (preg_match('/epdqdata/', $response_lines[$i])) {
        $strEPDQ = $response_lines[$i];
    }
  }
  return $strEPDQ;
}

  // Sends a request to EPDQ and returns a response array.
function uc_epdq_pullpage($host, $usepath, $postdata = '') {

  //open socket to filehandle(epdq encryption cgi)
  $fp = fsockopen($host, 80, $errno, $errstr, 60);

  //check that the socket has been opened successfully
  if (!$fp) {
    print "$errstr ($errno)<br>\n";
  }
  else {
    //write the data to the encryption cgi
    fputs($fp, "POST $usepath HTTP/1.0\n");
    $strlength = strlen($postdata);
    fputs($fp, "Content-type: application/x-www-form-urlencoded\n" );
    fputs($fp, "Content-length: " . $strlength . "\n\n" );
    fputs($fp, $postdata . "\n\n" );

    //clear the response data
    $output = '';

    //read the response from the remote cgi
    //while content exists, keep retrieving document in 1K chunks
    while (!feof($fp)) {
        $output .= fgets($fp, 1024);
    }
    //close the socket connection
    fclose( $fp);
  }
  //return the response
  return $output;
}