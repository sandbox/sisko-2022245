<?php

variable_set('BarclaysFields', array('N/A', 'FirstName', 'LastName', 'AddressLine#1', 'AddressLine#2', 'AddressLine#3', 'Town/City', 'PostCode', 'Country', 'Email', 'Mobile#', 
				'Cost', 'Cost-Other', 'Payment-Status'));

/***************************
 * adding the dropdown list
 * of alms data to the 
 * relevant webforms
 ***************************/
function webform_BarclaysEPDQ_form_webform_component_edit_form_alter(&$form, &$form_state, $form_id){
	// dsm($form, 'form_alter');
	// drupal_set_message("BarclaysEPDQ form components settings area!");

	//component types to exclude
	$excludes = array('fieldset', 'markup');

	//exit if this component is on the $excludes list
	if( in_array($form['type']['#value'], $excludes) )
		return;

	/*************************************
	 * make sure ePdq component settings 
	 * are copied from template to this 
	 * node
	 * --------------------------------
	 * ignore if the node is a template 
	 * - templates have no hierachy data
	 * to inherit
	 ************************************/
	if( in_array( node_load($form['nid']['#value'])->type, getRegistrationNodeTypes() ) )
		_confirm_epdq_components_settings($form['nid']['#value'], $form['cid']['#value']);

  	$FIELDS = variable_get('BarclaysFields');


	$setting = _epdq_get_component_setting($form['nid']['#value'], $form['cid']['#value']);
	$default = $setting ? array_search($setting->type, $FIELDS) : 0;

	$form['barclays_epdq']['container'] = array(
		'#type' => 'fieldset',
		'#title' => t('Barclays ePDQ'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#weight' => 50,
	);
	$form['barclays_epdq']['container']['field_option'] = array(
		'#type' => 'select',
		'#title' => t('Barclays field options'),
		'#options' => $FIELDS,
		'#description' => t('<em>Select the type of data submitted to this component in relation to communicating with Barclays</em>.'),
		'#default_value' => $default,
	);

  $form['#submit'][] = '_barclays_webform_components_settings';

}

/******************************
 * checks if current node has
 * ePdq component settings.
 * Returns if it does, copies
 * them from template if it
 * doesn't
 ******************************/
function _confirm_epdq_components_settings($nid, $cid){
	$epdqForm = _epdq_get_components_settings($nid);
	// dsm($epdqForm);

	if( sizeof($epdqForm) == 0 ){
		drupal_set_message("No ePdq settings exist for node/{$nid}", 'error');

		$data = _webform_hierachy_actions_data($nid);
		$epdqTemplate = _epdq_get_components_settings($data->tid);
		dvm($epdqTemplate);
		foreach ($epdqTemplate as $obj) {
			_set_epdq_component($nid, $obj->cid, $obj->type);
		}
	}else{
		return; //exit if settings already exist
	}
	
}


function _barclays_webform_components_settings(&$form, &$form_state){
  $fields = variable_get('BarclaysFields');

  // dsm($form_state['values'], '_barclays_webform_components_settings');
  $nid 	= $form_state['values']['nid'];
  $cid 	= $form_state['values']['cid'];
  $type = $fields[ $form_state['values']['barclays_epdq']['container']['field_option'] ];

  _set_epdq_component($nid, $cid, $type);
}

function _set_epdq_component($nid, $cid, $type){
	if($type == 'N/A'){
		db_delete('webform_barclaysepdq_components')
			->condition('nid', $nid)
			->condition('cid', $cid)
			->execute();
	}else{
		db_merge('webform_barclaysepdq_components')
			->key(array('nid' => $nid))
			->key(array('cid' => $cid))
			->fields(array(
			    'nid' => $nid,
			    'cid' => $cid,
			    'type'=> $type
		))->execute();
	}
}

/******************************
 * retrieves settings relating
 * webform components to epdq
 * settings we have specified
 ******************************/
function _epdq_get_component_setting($nid, $cid){
	return db_select('webform_barclaysepdq_components', 'wbc')
	->fields('wbc')
	->condition('nid', $nid, '=')
	->condition('cid', $cid, '=')
	->execute()
	->fetch();
}
	/******************************
	 * retrieves actual data for 
	 * the specified NID & CID from
	 * webform settings
	 ******************************/
	function _epdq_webform_component_submitted_data($nid, $cid, $sid){
		// drupal_set_message("Searching on NID#{$nid}, CID#{$cid} & SID#{$sid}");
		$result = db_select('webform_submitted_data', 'wsd')
		->fields('wsd')
		->condition('nid', $nid, '=')
		->condition('cid', $cid, '=')
		->condition('sid', $sid, '=')
		->execute()
		->fetch();
		
		// dsm($result, 'result');

		if($result)
			return $result->data;
	}

function _epdq_get_components_settings($nid, $type=null){
	$query = db_select('webform_barclaysepdq_components', 'wbc')->fields('wbc');
	
	if( $type )
		$query->condition('type',	$type, '=');
	
	$query->condition('nid', $nid, '=');

	if( $type )
		return $query->execute()->fetch();
	else
		return $query->execute()->fetchAll();
}


function _epdq_mark_form_submission_as_paid($nid, $sid, $cid){
	db_update('webform_submitted_data')
		->fields(array(
    		'data' => 'paid',
    	))
  		->condition('nid', $nid)
    	->condition('sid', $sid)
    	->condition('cid', $cid)
    	->execute();
}