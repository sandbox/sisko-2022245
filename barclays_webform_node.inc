<?php
// module_load_include('inc', 'webform_BarclaysEPDQ', 'components_config');


function webform_BarclaysEPDQ_form_alter(&$form, &$form_state, $form_id){

	/**************************************
	 * making alterations to the node-edit
	 * form
	 **************************************/
	if( isset($form['#node_edit_form']) && $form['#node_edit_form'] ){

		// $global_nid = node_load($form['nid']['#value']);
		// variable_del('current_nid');

		/************************************
		 * setting current_nid so I can
		 * retrieve and use the value in
		 * the token and other hook that 
		 * don't have NID, or any, arguments
		 ************************************/
			variable_set('current_nid', $form['nid']['#value']);

	// dsm($form_state, 'checking tokens');

		// $edit_node = node_load($form['nid']['#value']);
		// dsm($edit_node, 'showing from webform_BarclaysEPDQ_form_alter');
		// dsm($form);

		//dismiss when the node is being newly created. Prevents a user from selecting an ePdq store which results in an error
		if( !isset($form_state['node']->nid) )
			return;

		//dismiss any nodes which are NOT in the Barclays node types set
		if( !in_array($form['type']['#value'], get_barclaysepdq_nodetypes()) )
			return;
		
		drupal_flush_all_caches();

		$settings		=	get_barclaysepdq_node_settings($form['nid']['#value']);
		// dpr($settings);
		$active_state	=	sizeof($settings) > 0 ? 1 : 0;
		#drupal_set_message("DEFAULT VALUE: {$active_state}");

		$form['barclays_epdq'] = array(
			'#type' => 'fieldset',
			'#access' => TRUE,
			'#title' => 'Barclays ePDQ',
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
			'#group' => 'additional_settings',
			'#weight' => 100,
		);

		$stores	 =	get_barclaysepdq_settings();

		$options = array();
		$options['not_applicable'] = 'N\A';
		foreach ($stores as $store) {
			$options[$store->sid] = $store->name;
		}
// dvm($options);
		$form['barclays_epdq']['barclays_epdq_selected_storeid'] = array(
			'#type' => 'select',
			'#title' => t('Select an ePDQ store'),
			'#options' => $options,
			'#description' => t('Set this to a pre-set Barclays ePDQ storeID. <b>Set to N/A to remove existing setting</b>'),
			// '#default_value' => 'not_applicable',
		);
		//check if settings for this node exist and assign the previously selected dropdown option, if it does
		if( $settings )
			$form['barclays_epdq']['barclays_epdq_selected_storeid']['#default_value']	=	$settings->sid;

		$form['#submit'][] = '_webform_BarclaysEPDQ_form_alter_submit';
		

	  // Or use it in a form:
	  $form['barclays_epdq']['container'] = array(
	    '#theme' 		=> 'token_tree',
	    '#token_types' 		=> array('webform_epdq'),
	    '#global_types' => FALSE,
	    // '#token_types' => $components,
	  );
	}else{
		// barclaysCall($form, $form_state);
		// $form['#submit'][] = '_webform_BarclaysEPDQ_form_FINAL_submit';
	}
	
	return $form;
}

function barclaysCall($form=null, $form_state=null){
			// dsm($form, 'FORM: in Barclays-Call function!');
			// dsm($form_state, 'FORM-STATE: in Barclays-Call function!');

	/**************************************
	 * adding the custom ePDQ hidden form
	 * components for redirection to 
	 * Barclays
	 **************************************/
	/*if( isset($form['#node']) && in_array($form['#node']->type, getRegistrationNodeTypes()) 
		&& isset($form_state['webform']['page_num']) && isset($form_state['webform']['page_count']) ){*/

	if( isset($form['#node']) && isset($form_state['webform']['page_num']) && isset($form_state['webform']['page_count']) ){

		if( $form_state['webform']['page_num']  == $form_state['webform']['page_count'] ){
			drupal_set_message("LAST PAGE!", 'warning');
		}// close last page check
	}// close correct node-type check

	return $form;
}

function epdqform($form=null, $epdqSettings){
  module_load_include('inc', 'webform_BarclaysEPDQ', 'epdqOLD');
  return setForm($form, $epdqSettings);
}

/*********************
 * inserting settings
 * in database
 *********************/
function _webform_BarclaysEPDQ_form_alter_submit(&$form, &$form_state){

	$data = $form_state['values'];

	if( isset($data['barclays_epdq_selected_storeid']) ){
		if( is_numeric($data['barclays_epdq_selected_storeid']) ){
			
			drupal_set_message("INSERTING/UPDATING Barclay-ePDQ setting for <u>Node#{$data['nid']}</u> & <u>storeID#{$data['barclays_epdq_selected_storeid']}</u>", 'warning');
			_settings_insert($data['nid'], $data['barclays_epdq_selected_storeid']);

	    }else if( $data['nid'] && $data['barclays_epdq_selected_storeid'] == 'not_applicable' ){
			
			drupal_set_message("DELETING Barclay-ePDQ setting for <u>Node#{$data['nid']}</u>", 'error');
			watchdog("Barclay-ePDQ", "Deleting store setting for {$data['title']}.", array(), WATCHDOG_NOTICE);
			_settings_delete($data['nid']);

	    }
	}
}

/*********************
 * redirecting to 
 * barclays upon form
 * submission
 *********************/
/*function _webform_BarclaysEPDQ_form_FINAL_validate(&$form, &$form_state){
	// return TRUE;
}
function _webform_BarclaysEPDQ_form_FINAL_submit(&$form, &$form_state){
	// dsm($form_state, 'Form-State');
}*/

function webform_BarclaysEPDQ_node_view($node, $view_mode, $langcode){
	// drupal_set_message("Viewing in <u>{$view_mode}</u> mode!");

	/*************************************
	 * make sure ePdq component settings 
	 * are copied from template to this 
	 * node
	 * --------------------------------
	 * ignore if the node is a template 
	 * - templates have no hierachy data
	 * to inherit
	 ************************************/
	if( $view_mode == 'full' && in_array( $node->type, getRegistrationNodeTypes()) )
		_confirm_epdq_components_settings($node->nid, 0);
}

/*********************************
 * executing the settings insert
 * process as a function so it 
 * can be used elsewhere
 ********************************/
function _settings_insert($nid, $sid){
	db_merge('webform_barclaysepdq_settings')
		->key(array('nid' => $nid))
		->fields(array(
		    'nid' => $nid,
		    'sid' => $sid
		))->execute();
}

/*********************************
 * executing the settings delete
 * process as a function so it 
 * can be used elsewhere
 * ---------------------------
 * also delete the epdq component
 * settings
 ********************************/
function _settings_delete($nid){
	db_delete('webform_barclaysepdq_settings')
		      ->condition('nid', $nid)
		      ->execute();
	db_delete('webform_barclaysepdq_components')
		      ->condition('nid', $nid)
		      ->execute();
}

/*********************************
 * intercepting the node save
 * process and coping over the 
 * templates ePDQ store data for
 * the incomming event node
 ********************************/
function webform_BarclaysEPDQ_node_insert($node){
	$settings = get_barclaysepdq_node_settings($node->source);
	if($settings)
		_settings_insert($node->nid, $settings->sid);
}

/*********************************
 * removing ePDQ settings for
 * current node upon delete
 ********************************/
function webform_BarclaysEPDQ_node_delete($node){
	if( in_array($node->type, get_barclaysepdq_nodetypes()) )
		_settings_delete($node->nid);
}

function webform_BarclaysEPDQ_token_info() {
	// token_clear_cache();
	$current_nid = variable_get('current_nid', 0);
	// dsm("TESTING .... TESTING {$current_nid}", 'warning');
	$node = node_load($current_nid);

  $types['webform_epdq'] = array(
    'name' => t("ePDQ Data"),
    // 'description' => t("Tokens relating to ePDQ enable webforms."),
	// 'needs-data' => 'array',
  );
  
  	$webform_epdq = array();
	if( isset($node->webform['components']) ){
		foreach ($node->webform['components'] as $component) {
			// $key = "TOKEN_{$component['form_key']}";
			// $val = "VALUE_{$component['name']}";
			$webform_epdq[$component['form_key']] = array(
				'name' => $component['name'],
				// 'description' => t("data for the {$component['name']} field of this form"),
			);
		}
	}
	// dvm($webform_epdq, 'checking webform_epdq content');
	if( !empty($webform_epdq) ){
		return array(
			'types' => $types,
			'tokens' => array(
			  'webform_epdq' => $webform_epdq,
			),
		);
	}
  
}

function webform_BarclaysEPDQ_tokens($type, $tokens, $data = array(), array $options = array()) {

	// module_load_include('inc', 'webform_BarclaysEPDQ', 'components_config');
	// dsm($options, 'options');
		// $barclays_data = _barclaysform_submitted_data($options['nid'], $options['sid']);
		// 	dsm($barclays_data, 'barclays_data');



	/***************************************************************/
	$url = drupal_get_query_parameters(null, array());
	// dsm($url['q'], 'checking TOKEN type');

	$url = explode('/', $url['q']);
	if($url[0] == 'payment_status'){
		$nid = $url[1];
		$sid = $url[2];
	}

  $replacements = array();
  	
  if ($type == 'webform_epdq') {
	// dvm($tokens);
    foreach ($tokens as $name => $original) {
		// drupal_set_message("TESTING: {$name} - {$original}");
		$parts = explode(':', $original);
		$key = substr($parts[1], 0, -1);
		// drupal_set_message("TESTING: {$key}", 'warning');
      switch ($name) {
        /*case 'title':
          $replacements[$original] = 'TITLE';
        break;
        case 'last_name':
          $replacements[$original] = 'LAST-NAME';
        break;
        case 'first_name':
          $replacements[$original] = 'FIRST-NAME';
        break;*/
        default:
			$result = _barclaysform_submitted_data($nid, $sid, $key);
        	$replacements[$original] = $result->data;
        break;
      }
    }
  }
	/*$amount	= form_data('Cost', $nid, $sid, true);
	$other 	= form_data('Cost-Other', $nid, $sid, true);
	dvm($amount, 'checking amount');
	dvm($other, 'checking other-amount');
	$barclays_data = _barclaysform_submitted_data($options['nid'], $options['sid']);
	foreach ($barclays_data as $key => $value) {
		if($value->cid == $amount['cid'])
			break;
	}
	
	// dsm($value, 'chacking other');
	$amount_token = "[webform_epdq:{$value->form_key}]";
	drupal_set_message("Testing label: {$amount_token}", 'warned');

	if(isset($amount) && isset($other)){
		unset($replacements[$amount_token]);
		dpr($amount, 'amount');
		drupal_set_message("Testing label: {$amount_token}", 'warning');
	}*/

  // dsm($replacements);
  return $replacements;
}

/*******************************
 * fetch a nodes' data as
 * relating to webform 
 * questionnaire validation
 ********************************/
	function _barclaysform_submitted_data($nid=0, $sid=null, $form_key=null){
		#if($cid	>	0){
		#drupal_set_message("TESTING : {$cid}");
		$query	=	db_select('webform_component', 'wc');
					$query->join('webform_submitted_data', 'wsd', 'wsd.cid = wc.cid');
					$query->fields('wc');
					$query->fields('wsd', array('data'));
					$query->condition('wc.nid', $nid, '=');
					$query->condition('wsd.sid', $sid, '=');
					if($form_key)
						$query->condition('wc.form_key', $form_key, '=');

					// if($cid)
						// $query->condition('wqf.cid', $cid, '=');
					// $query->groupBy('wqf.cid');

					if($form_key)
						$result	=	$query->execute()->fetch();
					else
						$result	=	$query->execute()->fetchAll();
		// dsm($result, 'barclaysform_submitted_data');
		return $result;
	}

/**************************
 * matching settings to
 * submitted data and 
 * return form data
 **************************/
function form_data($find=null, $formNID=0, $formSID=0, $focused=false){
// variable_set('BarclaysFields', array('N/A', 'FirstName', 'LastName', 'AddressLine#1', 'AddressLine#2', 'AddressLine#3', 'Town/City', 'PostCode', 'Country', 'Email', 'Mobile#', 'Cost'));

  $epdqSettings   = _epdq_get_components_settings($formNID);
  foreach ($epdqSettings as $setting) {
    if($setting->type == $find)
      break;
  }

  $submission = webform_menu_submission_load($formSID, $formNID);
  // dpr($submission);
  // print"<pre>";
  // print_r($setting);
  // print"</pre>";
  if($submission){
    // drupal_set_message("TESTING {$find}: {$submission->data[$setting->cid][0]}", 'error');
    if($focused)
    	return array('cid'=>$setting->cid, 'value'=>$submission->data[$setting->cid][0]);
    else
    	return $submission->data[$setting->cid][0];
  }
}

// http://164.177.146.240/eventbookingNewDemo1/payment_status/274/957?oid=919
// http://164.177.146.240/eventbookingNewDemo1/payment_status/277/961?oid=923#

	// http://164.177.146.240/eventbookingNewDemo1/payment_status/274/976?oid=934