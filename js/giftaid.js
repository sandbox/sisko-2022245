(function ($) {

  // console.debug(Drupal.settings.webform_BarclaysEPDQ);

  $check      = Drupal.settings.webform_BarclaysEPDQ.check;
  $total      = Drupal.settings.webform_BarclaysEPDQ.total;
  $amount     = Drupal.settings.webform_BarclaysEPDQ.amount;
  $percentage = Drupal.settings.webform_BarclaysEPDQ.percentage;
  // $amount     = parseFloat($amount).toFixed(2);

  $($total).html( parseFloat($amount).toFixed(2) );
  $($total).css('background-color', 'yellow');

  $($check).click(
    function(){
      if( $(this).attr('checked') == true ){
        $gift_aided = parseFloat($amount) + parseFloat(($amount * $percentage) / 100);
        $($total).html( $gift_aided.toFixed(2) );
      }else{
        $($total).html( parseFloat($amount).toFixed(2) );
      }

  });

})(jQuery);