<?php

/*************************************************************
 * General
 *************************************************************/
	/*function eureka_rule_rules_event_info() {
	  return array(
	    'example_rule_event' => array(
	      'label' => t('Example rule event'),
	      'module' => 'eureka_rule',
	      'group' => 'Eureka Rule' ,
	      'variables' => array(
	        'current_user' => array('type' => 'user', 'label' => t('The current logged in user.')),
	        'article' => array('type' => 'node', 'label' => t('The article node.')),
	        'some_text' => array('type' => 'text', 'label' => t('Some arbitray text.')),
	      ),
	    ),
	  );
	}*/

	function webform_BarclaysEPDQ_rules_event_info() {

	  $items['webform_viewed'] = array(
	      'label' => t('Webform content is viewed'),
	      'group' => t('Webform'),
	      'variables' => webform_viewed() + array(
	        'webform' => array(
	          'type' => '*',
	          'label' => t('Current Webform'),
	      ),
	    ),
	  );
	  $items['payment_feedback'] = array(
			'label' => t('Payment Feedback is receieved'),
			'group' => t('Barclays ePDQ'),
			'variables' => array(
				'current_uri' => array(
					'type' => 'text',
					'label' => t('Current URI'),
				),
				'node' => array(
					'type' => 'node',
					'label' => t('The submitted webform-node'),
				),
				'target_email' => array(
					'type' => 'text',
					'label' => t('Rececient email address'),
				),
				'payment_status' => array(
					'type' => 'text',
					'label' => t('Payment Status'),
				),
			),
      		// 'access callback' => 'viewing_payment_feedback_event',
		);
	  return $items;
	}
	function payment_feedback(){
		drupal_set_message("payment_feedback Event function Responding!", 'warned');
		die('payment_feedback');
	}
/****************************************************************************/
	function webform_BarclaysEPDQ_rules_condition_info() {
		$contitions['barclays_condition_nodetype_compare'] = array(
			'label' => t('Content is Barclays-enabled'), 
			'group' => t('Webform'),
				'parameter' => array(
					'type' => array(
					'label' => t('Form Type'),
					'type' => 'text',
				),
			), 
		);
		$contitions['webform_on_next_page_check'] = array(
			'label' => t('Webform is on next page'), 
			'group' => t('Webform'),
			'parameter' => array(
					'form' => array(
						'type' => '*',
						'label' => t('Form'),
				),
					'page' => array(
						'type' => 'integer',
						'label' => t('Target page number'),
				),
			),
		);
		$contitions['viewing_payment_feedback_condition'] = array(
			'label' => t('Viewing payment feedback'), 
			'group' => t('Barclays ePDQ'),
			'parameter' => array(
				'feedbackurl' => array(
					'type' => 'text',
					'label' => t('URL of payment-feedback page'),
				),
			),
		);
		return $contitions;
	}

/*************************************************************
 * Gift-Aid related Rules 
 *************************************************************/

	function webform_viewed() {
	  return array(
	    'user' => array(
	      'type' => 'user',
	      'label' => t('User, who submitted the webform'),
	    ),
	    'node' => array(
	      'type' => 'node',
	      'label' => t('The webform node'),
	    ),
	    'data' => array(
	      'type' => 'webform',
	      'label' => t('The submitted webform data'),
	    ),
	    'form_id' => array(
	      'type' => 'text',
	      'label' => t('The form id of the submitted form'),
	      'hidden' => TRUE,
	    ),
	    'selected_webform' => array(
	      'type' => 'list<text>',
	      'label' => t('Webforms'),
	      'description' => t('The name of the webform being viewed.'),
	      'restriction' => 'input',
	      'hidden' => TRUE,
	      'optional' => TRUE,
	    ),
	  );
	}



function barclays_condition_nodetype_compare($type = null){
	/*dsm($type, 'TYPE');
	drupal_set_message("RULE Barclays-enabled?: " . in_array($type, get_barclaysepdq_nodetypes()) );*/

	if( in_array($type, get_barclaysepdq_nodetypes()) )
		return TRUE;
	return False;
}
function webform_on_next_page_check($form=null, $page=null){
	// dsm($form, 'data webform on next page');
	if( $page == $form->content['webform']['#form']['details']['page_num']['#value'] ){
		// drupal_set_message("We are on right page");
		return TRUE;
	}
	// drupal_set_message("We are <u>NOT</u> on right page");

}

		function webform_BarclaysEPDQ_rules_action_info() {
			$actions['webform_activate_giftaid'] = array(
				'label' => t('Gift-Aid related components'), 
				'group' => t('Webform'),
				'parameter' => array(
					'form' => array(
						'type' => '*',
						'label' => t('Form'),
					),
					'checkbox' => array(
						'type' => 'text',
						'label' => t('Target component to activate Gift-Aid calculation'),
						'description' => 'CSS/jQuery style path to checkbox component',
					),
					'totalField' => array(
						'type' => 'text',
						'label' => t('Target component to include/exclude Gift-Aid'),
						'description' => 'CSS/jQuery style path to total component',
					),
					'percentage' => array(
						'type' => 'integer',
						'label' => t('Number representing a percentage to calculate as Gift-Aid'),
						'description' => 'Gift-Aid percentage',
					),
				),
			);
			$actions['cash_donor_amount_input_settings'] = array(
				'label' => t('Cash Donor Input'), 
				'group' => t('Webform'),
				'parameter' => array(
					'form' => array(
						'type' => '*',
						'label' => t('Form'),
					),
					'amount' => array(
						'type' => 'text',
						'label' => t('Target amount component'),
						'description' => 'CSS/jQuery style path to amount component',
					),
					'otheramount' => array(
						'type' => 'text',
						'label' => t('Target other-amount component'),
						'description' => 'CSS/jQuery style path to other-amount component',
					),
				),
			);
			$actions['barclays_epdq_fetch_payment_feedback_data_action'] = array(
				'label' => t('Send payment status'), 
				'group' => t('Barclays ePDQ'),
				'parameter' => array(
					'message' => array(
						'type' => '*',
						'label' => t('Message data'),
				'save' => TRUE,
					),
					/*'nid' => array(
						'type' => '*',
						'label' => t('Form ID'),
					),
					'sid' => array(
						'type' => 'text',
						'label' => t('Form submission ID'),
					),
					'oid' => array(
						'type' => 'text',
						'label' => t('Payment ID'),
					),*/
				),
			);
			return $actions;
		}

		function barclays_epdq_fetch_payment_feedback_data_action($message=null){//$nid=null, $sid=null, $oid=null){
			drupal_set_message("Testing ePDQ feedback action: {$message}");
		}

		function webform_activate_giftaid($form=null, $checkbox=null, $totalField=null, $percentage=null){
			// dsm($form, 'Form - Gift-Aid action!');
			$amount	= _epdq_get_components_settings($form->nid, 'Cost');
			$other	= _epdq_get_components_settings($form->nid, 'Cost-Other');
			
			// dsm($form->content['webform']['#form']['#submission'], 'sumission data');

			$amount = $form->content['webform']['#form']['#submission']->data[$amount->cid][0];
			$other  = $form->content['webform']['#form']['#submission']->data[$other->cid][0];

			$amount = $other ? $other : $amount;

			/*dsm($amount, 'TESTing amount');
			dsm($other, 'Testing Other');*/

			// drupal_set_message("Retrieved {$amount} and {$other}", 'warned');
			
			drupal_add_js(drupal_get_path('module', 'webform_BarclaysEPDQ') .'/js/giftaid.js', array('type' => 'file', 'scope' => 'footer'));
			// $fields['check'] = $checkbox;
			// $fields['total'] = $totalField;
            drupal_add_js(array('webform_BarclaysEPDQ' => array('check' 	 => $checkbox)), 	'setting');
            drupal_add_js(array('webform_BarclaysEPDQ' => array('total' 	 => $totalField)), 	'setting');
            drupal_add_js(array('webform_BarclaysEPDQ' => array('amount' 	 => $amount)), 		'setting');
            drupal_add_js(array('webform_BarclaysEPDQ' => array('percentage' => $percentage)), 	'setting');

		}

		function cash_donor_amount_input_settings($form=null, $amount=null, $other=null){
			drupal_add_js(drupal_get_path('module', 'webform_BarclaysEPDQ') .'/js/amount.js', array('type' => 'file', 'scope' => 'footer'));
			drupal_add_js(array('webform_BarclaysEPDQ' => array('amount'	=> $amount)),	'setting');
            drupal_add_js(array('webform_BarclaysEPDQ' => array('other' 	=> $other)),	'setting');

		}

/*************************************************************
 * Email-response-to-payments related Rules 
 *************************************************************/
	/*function viewing_payment_feedback_event() {
		drupal_set_message("EVENT: Testing ePDQ payment Feedback");
	}*/
	function webform_BarclaysEPDQ_rules_invoke_event(){//$submission=null){
		drupal_set_message("Payment Feedback Event Firing");
	}
	function viewing_payment_feedback_condition($feedbackurl=null) {
		// die('testing rule condition response');
		/*drupal_flush_all_caches();
		dsm($_SERVER);
		drupal_set_message("CONDITION: Testing ePDQ payment Feedback: {$feedbackurl}");

		dvm($_GET['q']);

		$currentpath = request_uri();
		drupal_set_message("PATH#1: Testing current path: {$currentpath}");
		
		$currentpath = current_path();
		drupal_set_message("PATH#2: Testing current path: {$currentpath}");

		$currentpath = request_path();
		drupal_set_message("PATH#3: Testing current path: {$currentpath}");

		$currentpath = $_SERVER['REDIRECT_URL'];
		drupal_set_message("PATH#4: Testing current path: {$currentpath}");*/

		return TRUE;
	}