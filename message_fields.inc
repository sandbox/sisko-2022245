<?php

/******************************
 * called from 
 * questionnaire_nodetypes.inc
 ******************************/
function _set_fields_on_barclays_enabled_nodes($type){
	_set_field('epdq_success', 			'Post-ePDQ Success Message', 	'post-payment success message', 		$type);
	_set_field('epdq_un_successful', 	'Post-ePDQ Un-Success Message', 'post-payment un-successful message', 	$type);
}
/*function _set_fields_on_form_nodes($type){
	_set_field_textfield('alms_reference', 'ALMs Reference', 'ALMs Reference for this form', $type);
	_set_field_textfield('group_reference', 'Group Reference', 'ALMs Group Reference for this form', $type);
}*/

function _set_field($field, $label, $message, $type){
	$field_name = $field; // 32 characters limit
	
	$field = field_info_field($field_name);
	#dsm($field);
	#drupal_set_message("STATE : " . empty($field));
	if (empty($field)) {
		$field = array(
		  'field_name' => $field_name,
		  'type' => 'text_long',
		  'cardinality' => 1,
		  'settings' => array(),
		  'entity_types' => array('node'),
		  'locked' => FALSE,
		  'translatable' => TRUE,
		);
		field_create_field($field);
	#}
	
	#dsm( field_read_field($field_name) );
#drupal_set_message("APPLYING TO : {$nodetype}");
		// Attach the field to the node type
		$instance = array(
			'field_name' => $field['field_name'],
			'entity_type' => 'node',
			'bundle' => $type,
			'label' => t($label),
			'description' => t($message),
			'required' => TRUE,
			'widget' => array(
				'type' => 'text_textarea',
				'settings' => array(
				  'rows' => 10,
				  'summary_rows' => 3,
				),
				'weight' => 0,
			),
			'settings' => array(
				'display_summary' => TRUE,
				'text_processing' => 1, // @TODO Find out the name
			),
			'default_value' => array(),
			'display' => array(
				'default' => array(
				  'label' => 'hidden',
				  'type' => 'text_default',
				  'settings' => array(),
				  'weight' => 0,
				),
			),
		);
		field_create_instance($instance);
	}#close if (empty($field))
}

/**************************************
 * Using node reference for
 * Terms and Conditions page
 **************************************/
/*function _set_ref_field($field, $label, $message, $type){
	$field_name = $field; // 32 characters limit
	
	$field = field_info_field($field_name);
	#dsm($field);
	#drupal_set_message("STATE : " . empty($field));
	if (empty($field)) {
		$field = array(
		  'field_name' => $field_name,
		  'type' => 'node_reference',
		  'cardinality' => 1,
		  'settings' => array(),
		  'entity_types' => array('node'),
		  'locked' => FALSE,
		  'translatable' => TRUE,
		);
		field_create_field($field);
	#}
	
	#dsm( field_read_field($field_name) );
#drupal_set_message("APPLYING TO : {$nodetype}");
		// Attach the field to the node type
		$instance = array(
			'field_name' => $field['field_name'],
			'entity_type' => 'node',
			'bundle' => $type,
			'label' => t($label),
			'description' => t($message),
			'required' => FALSE,
			'widget' => array(
				'type' => 'node_reference',
				'settings' => array(
				  'rows' => 10,
				  'summary_rows' => 3,
				),
				'weight' => 0,
			),
			'settings' => array(
				'display_summary' => TRUE,
				'text_processing' => 1, // @TODO Find out the name
			),
			'default_value' => array(),
			'display' => array(
				'default' => array(
				  'label' => 'hidden',
				  'type' => 'text_default',
				  'settings' => array(),
				  'weight' => 0,
				),
			),
		);
		field_create_instance($instance);
	}#close if (empty($field))
}
*/
/*function _set_field_textfield($field, $label, $message, $type){
	$field_name = $field; // 32 characters limit
	
	$field = field_info_field($field_name);
	#dsm($field);
	#drupal_set_message("STATE : " . empty($field));
	if (empty($field)) {
		$field = array(
		  'field_name' => $field_name,
		  'type' => 'text',
		  'cardinality' => 1,
		  'settings' => array(),
		  'entity_types' => array('node'),
		  'locked' => FALSE,
		  'translatable' => TRUE,
		);
		field_create_field($field);
	#}
	
	#dsm( field_read_field($field_name) );
#drupal_set_message("APPLYING TO : {$nodetype}");
		// Attach the field to the node type
		$instance = array(
			'field_name' => $field['field_name'],
			'entity_type' => 'node',
			'bundle' => $type,
			'label' => t($label),
			'description' => t($message),
			'required' => TRUE,
			'widget' => array(
				'type' => 'textfield',
				'settings' => array(
				  'rows' => 10,
				  'summary_rows' => 3,
				),
				'weight' => 0,
			),
			'default_value' => array(),
			'display' => array(
				'default' => array(
				  'label' => 'hidden',
				  'type' => 'text_default',
				  'settings' => array(),
				  'weight' => 5,
				),
			),
		);
		field_create_instance($instance);
	}#close if (empty($field))
}*/